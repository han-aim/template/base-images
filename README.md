# Base images<!-- omit in toc -->

Use these container images as base layers.
These images help with standardization, security and efficiency.

## To build

See [AIM Tools - To build](https://gitlab.com/han-aim/aimtools#to-build).
