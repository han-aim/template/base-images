[build-system]
build-backend = "pdm.backend"
requires = ["pdm-backend"]

[dependency-groups]
build = [
  "pdm-backend>=2.4.3",
]
dev = [
  "aimtools==2024.12.4.8.36.15.203.0.0",
  "black[jupyter]>=24.10.0",
  "cibuildwheel>=2.22.0",
  "coverage>=7.6.8",
  "creosote>=3.2.0",
  "debugpy>=1.8.9",
  "isort>=5.13.2",
  "lefthook==1.8.5",
  "linkify-it-py>=2.0.3",
  "loguru-mypy>=0.0.4",
  "mdformat-black>=0.1.1",
  "mdformat-footnote>=0.1.1",
  "mdformat-frontmatter>=2.0.8",
  "mdformat-gfm>=0.3.7",
  "mdformat-pyproject>=0.0.2",
  "mdformat-shfmt>=0.2.0",
  "mdformat-tables>=1.0.0",
  "mdformat>=0.7.19",
  "mypy>=1.13.0",
  "nbconvert>=7.16.4",
  "pdm==2.22.0",
  "pylint>=3.3.2",
  "pytest-asyncio>=0.24.0",
  "pytest>=8.3.4",
  "python-dotenv[cli]>=1.0.1",
  "ruff>=0.8.1",
  "toml-sort>=0.24.2",
  "uv==0.5.7",
]

[project]
authors = [
  {email = "Sander.Maijers@han.nl", name = "Sander Maijers"},
]
classifiers = [
  "Programming Language :: Python :: 3.12",
]
dependencies = []
description = ""
dynamic = ["version"]
license = {text = "NONE"}
name = "baseimages"
readme = "README.md"
requires-python = ">=3.12"

[tool]

[tool.black]
include = '\.pyi?$'
line-length = 120

[tool.creosote]
exclude-deps = [
  "aimtools", # Implicit (CLI) dependency.
  "defusedxml", # Implicit optional dependency of openpyxl.
  "python-multipart",
  "uvicorn", # Non-source code dependency.
]

[tool.isort]
atomic = true
case_sensitive = true
extra_standard_library = ["typing_extensions"]
include_trailing_comma = true
line_length = 120
multi_line_output = 3
profile = "black"
py_version = "auto"
remove_redundant_aliases = true
skip_gitignore = true

[tool.mypy]
disallow_any_generics = true
disallow_incomplete_defs = true
disallow_untyped_calls = true
disallow_untyped_decorators = true
disallow_untyped_defs = true
exclude = [
  '^\.venv/.*$',
  '^\_template/.*$',
]
namespace_packages = false
no_implicit_reexport = true
show_error_codes = true
show_error_context = true
strict_equality = true
strict_optional = true
warn_redundant_casts = true
warn_return_any = true
warn_unreachable = true
warn_unused_ignores = true

[[tool.mypy.overrides]]
ignore_missing_imports = true
module = [
  "diceware.*",
  "dotenv.*",
  "kubernetes.*",
  "msgraph.*",
  "pathvalidate.*",
  "plotly.*",
  "pyroute2.*",
  "supervision.*",
  "ultralytics.*",
]

[tool.pdm]
distribution = true

[tool.pdm.build]
is-purelib = true

[tool.pdm.resolution]
excludes = [
  "ruamel-yaml-clib",
]
no-binary = [
  "eccodes",
  "findlibs",
  "psutil",
  "timezonefinder",
  "utm",
]
only-binary = [
  ":all:",
]

[[tool.pdm.source]]
exclude_packages = ["*"]
include_packages = ["aimtools"]
name = "aimtools"
url = "https://gitlab.com/api/v4/projects/50625041/packages/pypi/simple"

[tool.pdm.version]
fallback_version = "0.0.0"
source = "scm"
# TODO: infosec
write_template = '''__version__ = "{}"
'''
write_to = "package/__version__.py"

[tool.pylint.format]
max-line-length = 120

[tool.pylint."messages control"]
disable = [
  "all",
]
enable = [
  "abstract-class-instantiated",
  "abstract-method",
  "access-member-before-definition",
  "anomalous-unicode-escape-in-string",
  "arguments-differ",
  "arguments-out-of-order",
  "arguments-renamed",
  "assigning-non-slot",
  "assignment-from-no-return",
  "assignment-from-none",
  "attribute-defined-outside-init",
  "bad-builtin",
  "bad-except-order",
  "bad-exception-cause",
  "bad-file-encoding",
  "bad-indentation",
  "bad-mcs-classmethod-argument",
  "bad-mcs-method-argument",
  "bad-reversed-sequence",
  "bad-staticmethod-argument",
  "bad-super-call",
  "bad-thread-instantiation",
  "catching-non-exception",
  "chained-comparison",
  "class-variable-slots-conflict",
  "compare-to-zero",
  "comparison-with-callable",
  "condition-evals-to-constant",
  "confusing-consecutive-elif",
  "confusing-with-statement",
  "consider-swap-variables",
  "consider-using-assignment-expr",
  "consider-using-augmented-assign",
  "consider-using-dict-items",
  "consider-using-enumerate",
  "consider-using-f-string",
  "consider-using-from-import",
  "consider-using-join",
  "consider-using-max-builtin",
  "consider-using-min-builtin",
  "consider-using-namedtuple-or-dataclass",
  "consider-using-tuple",
  "consider-using-with",
  "cyclic-import",
  "deprecated-argument",
  "deprecated-class",
  "deprecated-decorator",
  "deprecated-method",
  "deprecated-module",
  "deprecated-typing-alias",
  "dict-init-mutate",
  "dict-iter-missing-items",
  "differing-param-doc",
  "differing-type-doc",
  "disallowed-name",
  "duplicate-code",
  "empty-comment",
  "global-at-module-level",
  "global-variable-undefined",
  "import-error",
  "import-private-name",
  "inconsistent-mro",
  "inherit-non-class",
  "invalid-bool-returned",
  "invalid-bytes-returned",
  "invalid-character-carriage-return",
  "invalid-characters-in-docstring",
  "invalid-class-object",
  "invalid-enum-extension",
  "invalid-envvar-value",
  "invalid-format-index",
  "invalid-format-returned",
  "invalid-getnewargs-ex-returned",
  "invalid-getnewargs-returned",
  "invalid-hash-returned",
  "invalid-index-returned",
  "invalid-length-hint-returned",
  "invalid-length-returned",
  "invalid-metaclass",
  "invalid-overridden-method",
  "invalid-repr-returned",
  "invalid-sequence-index",
  "invalid-slice-index",
  "invalid-slice-step",
  "invalid-slots",
  "invalid-slots-object",
  "invalid-star-assignment-target",
  "invalid-str-returned",
  "invalid-unary-operand-type",
  "invalid-unicode-codec",
  "isinstance-second-argument-not-valid-type",
  "logging-format-truncated",
  "logging-unsupported-format",
  "method-cache-max-size-none",
  "method-hidden",
  "misplaced-format-function",
  "missing-any-param-doc",
  "missing-format-attribute",
  "missing-kwoa",
  "missing-param-doc",
  "missing-parentheses-for-call-in-test",
  "missing-raises-doc",
  "missing-return-doc",
  "missing-return-type-doc",
  "missing-timeout",
  "missing-type-doc",
  "missing-yield-doc",
  "missing-yield-type-doc",
  "mixed-line-endings",
  "modified-iterating-dict",
  "modified-iterating-list",
  "modified-iterating-set",
  "multiple-constructor-doc",
  "nan-comparison",
  "no-member",
  "no-name-in-module",
  "no-value-for-parameter",
  "non-iterator-returned",
  "non-parent-init-called",
  "non-str-assignment-to-dunder-name",
  "nonlocal-and-global",
  "not-a-mapping",
  "not-an-iterable",
  "not-async-context-manager",
  "not-callable",
  "not-context-manager",
  "overlapping-except",
  "overridden-final-method",
  "pointless-string-statement",
  "possibly-unused-variable",
  "potential-index-error",
  "preferred-module",
  "raising-bad-type",
  "raising-format-tuple",
  "raising-non-exception",
  "redeclared-assigned-name",
  "redefined-outer-name",
  "redefined-slots-in-subclass",
  "redefined-variable-type",
  "redundant-keyword-arg",
  "redundant-returns-doc",
  "redundant-u-string-prefix",
  "redundant-unittest-assert",
  "redundant-yields-doc",
  "self-cls-assignment",
  "shallow-copy-environ",
  "signature-differs",
  "simplifiable-condition",
  "simplifiable-if-expression",
  "simplifiable-if-statement",
  "simplify-boolean-expression",
  "singledispatch-method",
  "singledispatchmethod-function",
  "star-needs-assignment-target",
  "stop-iteration-return",
  "subclassed-final-class",
  "super-init-not-called",
  "super-without-brackets",
  "superfluous-parens",
  "too-few-public-methods",
  "too-many-ancestors",
  "too-many-function-args",
  "too-many-instance-attributes",
  "too-many-lines",
  "too-many-nested-blocks",
  "too-many-try-statements",
  "trailing-newlines",
  "trailing-whitespace",
  "unbalanced-dict-unpacking",
  "unbalanced-tuple-unpacking",
  "undefined-loop-variable",
  "unexpected-keyword-arg",
  "unexpected-line-ending-format",
  "unhashable-member",
  "unnecessary-dunder-call",
  "unnecessary-ellipsis",
  "unpacking-non-sequence",
  "unreachable",
  "unsubscriptable-object",
  "unsupported-assignment-operation",
  "unsupported-binary-operation",
  "unsupported-delete-operation",
  "unsupported-membership-test",
  "unused-private-member",
  "unused-wildcard-import",
  "use-maxsplit-arg",
  "used-before-assignment",
  "useless-param-doc",
  "useless-parent-delegation",
  "useless-type-doc",
  "using-constant-test",
  "using-final-decorator-in-unsupported-version",
  "while-used",
  "wrong-exception-operation",
  "wrong-spelling-in-comment",
  "wrong-spelling-in-docstring",
]

[tool.pytest.ini_options]
asyncio_default_fixture_loop_scope = "function"
norecursedirs = ["_template"]
pythonpath = "src"
testpaths = [
  "tests",
]

[tool.ruff]
extend-exclude = ["_template"]
include = ["**/pyproject.toml", "*.ipynb", "*.py", "*.pyi"]
line-length = 120

[tool.ruff.lint]
allowed-confusables = ["‘", "’", "“", "”"]
# TODO: check for redundancy.
ignore = [
  "D",
  "D203",
  "D213",
  "EM101",
  "FIX002",
  "I001",
  "PT",
  "RSE102",
  "RUF001",
  "S101",
  "SLF001",
  "TD002",
  "TD003",
]
select = ["ALL"]

[tool.ruff.lint.flake8-annotations]
allow-star-arg-any = true

[tool.ruff.lint.per-file-ignores]
"[!s][!r][!c]*/**" = ["INP001"]
"__version__.py" = ["D100"]

[tool.tomlsort]
# In our context, arrays often are misused as sets, so their order doesn't matter.
# Sort all key-like entries lexicographically in increasing order.
all = true
trailing_comma_inline_array = true

[tool.uv.pip]
no-binary = [
  "eccodes",
  "findlibs",
  "psutil",
  "timezonefinder",
  "utm",
]
only-binary = [
  ":all:",
]
